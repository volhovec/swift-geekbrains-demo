//
//  ViewController.swift
//  Geekbrains
//
//  Created by Leonid Stefanenko on 21.10.2020.
//

import UIKit

class ViewController: UIViewController {
    
    var imageSize = 300
    let baseImageLink = "https://picsum.photos/"

    @IBOutlet weak var avatar: UIImageView!
    
    @IBOutlet weak var textField: UITextView!
    
    @IBOutlet weak var button300: UIButton!
    
    @IBOutlet weak var button400: UIButton!
    
    @IBOutlet weak var button500: UIButton!
    
    @IBAction func set300(_ sender: Any) {
        imageSize = 300
        button300.setTitleColor(UIColor.systemPink, for: UIControl.State.normal)
        button400.setTitleColor(UIColor.systemBlue, for: UIControl.State.normal)
        button500.setTitleColor(UIColor.systemBlue, for: UIControl.State.normal)
    }
    
    @IBAction func set400(_ sender: Any) {
        imageSize = 400
        button300.setTitleColor(UIColor.systemBlue, for: UIControl.State.normal)
        button400.setTitleColor(UIColor.systemPink, for: UIControl.State.normal)
        button500.setTitleColor(UIColor.systemBlue, for: UIControl.State.normal)
    }
    
    @IBAction func set500(_ sender: Any) {
        imageSize = 500
        button300.setTitleColor(UIColor.systemBlue, for: UIControl.State.normal)
        button400.setTitleColor(UIColor.systemBlue, for: UIControl.State.normal)
        button500.setTitleColor(UIColor.systemPink, for: UIControl.State.normal)
    }
    
    @IBAction func exitAction(_ sender: Any) {
        exit(-1);
    }
    
    @IBAction func getImage(_ sender: Any) {
        let imageLink = baseImageLink + String(imageSize)
        /*
         URLSession.shared.downloadTask(with: url!) { localUrl, response, error in
            var localData = try? Data(contentsOf: localUrl!)
            var image = UIImage(data: localData!)
            DispatchQueue.main.async {
                self.avatar.image = image
            }
         }.resume()
         */

        let url = URL(string: imageLink)
        let localData = try? Data(contentsOf: url!)
        let image = UIImage(data: localData!)
        DispatchQueue.main.async {
            self.avatar.image = image
            self.textField.text = "Image source: \n" + imageLink
        }
    }

}

